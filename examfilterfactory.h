/*************************************************************************************
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#ifndef EXAMFILTERFACTORY_H
#define EXAMFILTERFACTORY_H

#include <QString>
#include <QMap>

#define REGISTER_EXPORT_FILTER(name) \
        static AbstractExamExport* create##name() { return new name (); } \
        namespace { bool _##name=ExamFilterFactory::self()->registerExportFilter(create##name, name ::filter()); }

class AbstractExamExport;

class ExamFilterFactory
{
	public:
		typedef AbstractExamExport* (*registerFilter_fn)();
		static ExamFilterFactory* self();
		
		AbstractExamExport* filterForExtension(const QString& ext);
		bool registerExportFilter(registerFilter_fn func, const QString& filter);
		QStringList filters();
		QList<registerFilter_fn> exporters() const { return m_exportFilters.values(); }
		
	private:
		ExamFilterFactory();
		static ExamFilterFactory* m_self;
		
		QMap<QString, registerFilter_fn> m_exportFilters;
};

#endif
