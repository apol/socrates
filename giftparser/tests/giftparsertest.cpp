#include "giftparsertest.h"
#include <giftparser.h>
#include <QDebug>

QTEST_MAIN( GiftParserTest )

struct GiftTestVisitor : public AbstractGiftVisitor
{
	virtual void titleFound(const QString& title) { result += 't'; }
	virtual void textFound(const QString& text) { result += 'T'; }
    virtual void commentFound(const QString& comment) { result += 'c'; }
	
	virtual void openQuestion(bool numerical) { result += '{'; if(numerical) result+='#'; }
	virtual void closeQuestion() { result += '}'; }
	
	virtual void booleanAnswer(bool answer) { result += 'b'; }
	virtual void matchAnswer(const QString& from, const QString& to, const QString& feedback) { result += '>'; }
	virtual void numericalAnswer(bool interval, const QString& value, int percentage, const QString& rate, const QString& feedback) { result += '0'; }
	virtual void testAnswer(bool correct, int percentage, const QString& text, const QString& feedback) { result += 's'; }
	
	virtual void error(const QString& error) { qDebug() << "error:" << error; errors=true; }
	
	QString result;
	bool errors;
};

//t:title, T:text, c:comment, openQ:{#?, closeQ:}, bool:b=?, match:>, num:0, test:s

void GiftParserTest::giftTest()
{
	QFETCH(QString, question);
	QFETCH(QString, cont);
	
	GiftTestVisitor* visitor = new GiftTestVisitor;
	
	QTextStream ts(&question);
	GiftParser parser(&ts);
	parser.start(visitor);
	
	QCOMPARE(visitor->result, cont);
	QVERIFY(!visitor->errors);
}

void GiftParserTest::giftTest_data()
{
	QTest::addColumn<QString>("question");
	QTest::addColumn<QString>("cont");
	
	QTest::newRow("ex1") << "::Q1:: 1+1=2 {T}" << "tT{b}";
	QTest::newRow("ex2") << "::Q2:: What's between orange and green in the spectrum? {=yellow # correct! ~red # wrong, it's yellow ~blue # wrong, it's yellow}" << "tT{sss}";
	QTest::newRow("ex3") << "::Q3:: Two plus {=two =2} equals four." << "tT{ss}T";
	QTest::newRow("ex4") << "::Q4:: Which animal eats which food? { =cat -> cat food =dog -> dog food }" << "tT{>>}";
	QTest::newRow("ex5") << "::Q5:: What is a number from 1 to 5? {#3:2}" << "tT{#0}";
	QTest::newRow("ex6") << "::Q5:: What is a number from 1 to 5? {#1..5}" << "tT{#0}";
	QTest::newRow("ex7") << "::Q7:: When was Ulysses S. Grant born? {#"
		"=1822:0      # Correct! You get full credit."
		"=%50%1822:2  # He was born in 1822. You get half credit for being close."
		"}" << "tT{#00}";
	QTest::newRow("ex8") << "::Q8:: How are you? {}" << "tT{}";
	
	QTest::newRow("long") << "// question: 1 name: Grants tomb\n"
								"::Grants tomb::Who is buried in Grant's tomb in New York City? {\n"
								"=Grant\n"
								"~No one\n"
								"#Was true for 12 years, but Grant's remains were buried in the tomb in 1897\n"
								"~Napoleon\n"
								"#He was buried in France\n"
								"~Churchill\n"
								"#He was buried in England\n"
								"~Mother Teresa\n"
								"#She was buried in India\n"
							"}\n" << "ctT{sssss}";
							
	QTest::newRow("T") << "safadfhdsf sdf adsf" << "T";
	QTest::newRow("tq") << "safadfhdsf sdf adsf {}" << "T{}";
	QTest::newRow("tqT") << "safadfhdsf sdf adsf {} sdasdads" << "T{}T";
}

#include "giftparsertest.moc"
