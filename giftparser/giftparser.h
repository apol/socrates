#ifndef GIFTPARSER_H
#define GIFTPARSER_H

#include <QString>
#include <qtextstream.h>

class QTextStream;
class AbstractGiftVisitor
{
	public:
		virtual void titleFound(const QString& title)=0;
		virtual void textFound(const QString& text)=0;
		virtual void commentFound(const QString& comment)=0;
		
		virtual void openQuestion(bool numerical)=0;
		virtual void closeQuestion()=0;
		
		virtual void booleanAnswer(bool answer)=0;
		virtual void testAnswer(bool correct, int percentage, const QString& text, const QString& feedback)=0;
		virtual void matchAnswer(const QString& from, const QString& to, const QString& feedback)=0;
		
		/**
		 * @p interval. if true interval like from 1 to 2 (1..2), if false range like 1+-2 (1:2)
		 */
		virtual void numericalAnswer(bool interval, const QString& value, int percentage, const QString& range, const QString& feedback)=0;
		
		virtual void error(const QString& error)=0;
};

class GiftParser
{
	public:
		GiftParser(QTextStream* stream);
		void start(AbstractGiftVisitor* visitor);
		
	private:
		QString readUntil(const QString& delimiter);
		QString readUntil(const QChar& delimiter) { return readUntil(QString(delimiter)); }
		void parseQuestions(AbstractGiftVisitor* visitor);
		
		QChar getChar();
		bool isAtEnd() const { return m_stream->atEnd() && m_buff.isEmpty(); }
		
		QTextStream* m_stream;
		QString m_buff;
};

#endif // GIFTPARSER_H
