#include "giftparser.h"
#include <QTextStream>
#include <QRegExp>
#include <QDebug>
#include <QStringList>

GiftParser::GiftParser(QTextStream* stream)
	: m_stream(stream)
{}

QChar GiftParser::getChar()
{
	Q_ASSERT(!isAtEnd());
	QChar ret;
	if(m_buff.isEmpty())
		m_buff=m_stream->read(50);
	
	ret=m_buff[0];
	m_buff.remove(0,1);
	
	return ret;
}

void GiftParser::start(AbstractGiftVisitor* visitor)
{
	QString buff;
	for(; !isAtEnd(); ) {
		buff += getChar();
		
		if(buff.endsWith("::")) {
			buff.chop(2);
			if(!buff.isEmpty())
				visitor->textFound(buff);
			buff.clear();
			
			visitor->titleFound(readUntil("::"));
		} else if(buff.endsWith("//")) {
			buff.chop(2);
			if(!buff.isEmpty())
				visitor->textFound(buff);
			buff.clear();
			
			visitor->commentFound(readUntil('\n'));
		} else if(buff.endsWith('{')) {
			buff.chop(1);
			if(!buff.isEmpty())
				visitor->textFound(buff);
			buff.clear();
			
			parseQuestions(visitor);
		}
	}
	
	buff=buff.trimmed();
	if(!buff.isEmpty())
		visitor->textFound(buff);
}

QString GiftParser::readUntil(const QString& delimiter)
{
	QString buff;
	int escape=0;
	for(; !buff.endsWith(delimiter) && escape==0; ) {
		QChar ch = getChar();
		
		if(ch=='\\')
			escape=delimiter.size();
		else if(escape>0)
			--escape;
		else
			buff += ch;
	}
// 	qDebug() << "until" << buff << delimiter;
	buff.chop(delimiter.size());
	return buff;
}

static QRegExp rxBoolean("(T|F|TRUE|FALSE)");
static QRegExp rxTest ("\\s*([=|~])(%[0-9]{1,2}%)?([\\w\\d ]*)\\s*" "(#([^=|^~]*))?", Qt::CaseInsensitive, QRegExp::RegExp2);
static QRegExp rxMatch("\\s*=([\\w ]*) *-> *([\\w ]*)\\s*" "(#([^=]*))?", Qt::CaseInsensitive, QRegExp::RegExp2);

static QRegExp rxNumRange("\\s*=?(%[0-9]{1,2}%)?([\\d\\.]+)(:|\\.\\.)([\\d\\.]+)\\s*" "(#([^=]*))?");

void GiftParser::parseQuestions(AbstractGiftVisitor* visitor)
{
	QString answer = readUntil('}');
	
	bool isnumeric = answer[0]=='#';
	visitor->openQuestion(isnumeric);
	if(isnumeric) {
		answer[0]=' ';
		int last=-1;
		int i=rxNumRange.indexIn(answer);
// 		qDebug() << "bleee" << i << rxNumRange.capturedTexts() << rxNumRange.errorString() << answer;
		for(; i>=0; ) {
			QString percentage=rxNumRange.cap(1);
			int intpercentage = percentage.isEmpty()? 100 : percentage.remove('%').toInt();
			
			visitor->numericalAnswer(rxNumRange.cap(3)=="..", rxNumRange.cap(2), intpercentage, rxNumRange.cap(4), rxNumRange.cap(6).simplified());
			
			last=i+rxNumRange.matchedLength();
			i=rxNumRange.indexIn(answer, last);
		}
		
		if(last<answer.size())
			visitor->error(QString("Didn't recognize the full answer: %1").arg(answer.right(answer.size()-last)));
	} else if(rxBoolean.exactMatch(answer))
		visitor->booleanAnswer(rxBoolean.cap(1).startsWith('T'));
	else if(rxMatch.indexIn(answer)==0) {
// 		qDebug() << "captured match" << rxMatch.capturedTexts();
		int last=-1;
		for(int i=0; i>=0; ) {
			visitor->matchAnswer(rxMatch.cap(1), rxMatch.cap(2), rxMatch.cap(4));
			
			last=i+rxMatch.matchedLength();
			i=rxMatch.indexIn(answer, last);
		}
		if(last<answer.size())
			visitor->error(QString("Didn't recognize the full answer: %1").arg(answer.right(answer.size()-last)));
	} else if(rxTest.indexIn(answer)==0) {
// 		qDebug() << "captured test" << rxTest.capturedTexts();
		int last=-1;
		for(int i=0; i>=0; ) {
			visitor->testAnswer(rxTest.cap(1)=="=", rxTest.cap(2).remove('%').toInt(), rxTest.cap(3), rxTest.cap(5));
			
			last=i+rxTest.matchedLength();
// 			qDebug() << "left...." << answer.right(answer.size()-last) << last;
			i=rxTest.indexIn(answer, last);
		}
		if(last<answer.size())
			visitor->error(QString("Didn't recognize the full answer: %1").arg(answer.right(answer.size()-last)));
	} else if(!answer.isEmpty())
		visitor->error("couldn't match the question type for " + answer);
	
	visitor->closeQuestion();
}
