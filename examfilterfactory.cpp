/*************************************************************************************
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "examfilterfactory.h"
#include <QStringList>
#include "abstractexamexport.h"

ExamFilterFactory* ExamFilterFactory::m_self=0;

ExamFilterFactory* ExamFilterFactory::self()
{
	if(!m_self)
		m_self = new ExamFilterFactory;
	
	return m_self;
}

ExamFilterFactory::ExamFilterFactory()
{}

bool ExamFilterFactory::registerExportFilter(ExamFilterFactory::registerFilter_fn func, const QString& filter)
{
	m_exportFilters.insert(filter, func);
}

QStringList ExamFilterFactory::filters()
{
	return m_exportFilters.keys();
}

AbstractExamExport* ExamFilterFactory::filterForExtension(const QString& ext)
{
	AbstractExamExport* ret = 0;
	QMap<QString,registerFilter_fn>::const_iterator it=m_exportFilters.constBegin(), itEnd=m_exportFilters.constEnd();
	for(; !ret && it!=itEnd; ++it) {
		if(it.key().contains("*"+ext))
			ret = it.value()();
	}
	return ret;
}
