#ifndef EXAMENS_H
#define EXAMENS_H

#include <QtGui/QMainWindow>
#include "examensdriver.h"

class QItemSelection;
class QLabel;
class QListView;
class QModelIndex;
class QTreeView;

class Examens : public QMainWindow
{
	Q_OBJECT
	public:
		Examens();
		virtual ~Examens();
		
		
	public slots:
		void newQuestion();
		void currentChanged(const QModelIndex& idx, const QModelIndex& prev);
		void editQuestion(const QModelIndex& idx);
		void tagsSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
		void removeSelected();
		void exportExam();
		
	private:
		QTreeView* m_questionsView;
		QListView* m_tagsView;
		ExamensDriver m_data;
		QLabel* m_questionsInfo;
		QStringList m_lastTags;
};

#endif
