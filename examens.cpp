#include "examens.h"

#include <QtGui/QLabel>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QToolBar>
#include <QtGui/QTreeView>
#include <QtGui/QListView>
#include <QtGui/QDockWidget>
#include <QtSql/QSqlQueryModel>
#include <QMessageBox>
#include <QDebug>

#include "addquestion.h"
#include "generatewizard.h"
#include "examfilterfactory.h"
#include "abstractexamexport.h"

#define i18n(x) tr(x)

Examens::Examens()
{
	QToolBar* questionsToolbar = addToolBar(i18n("Questions"));
	questionsToolbar->addAction(QIcon::fromTheme("list-add"), i18n("Add Question"), this, SLOT(newQuestion()));
	questionsToolbar->addAction(QIcon::fromTheme("list-remove"), i18n("Remove Selected"), this, SLOT(removeSelected()));
	questionsToolbar->addSeparator();
	questionsToolbar->addAction(i18n("Export..."), this, SLOT(exportExam()));
	
	m_questionsView = new QTreeView(this);
	m_questionsView->setRootIsDecorated(false);
	m_questionsView->setModel(m_data.questionsModel());
	connect(m_questionsView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), SLOT(currentChanged(QModelIndex,QModelIndex)));
	connect(m_questionsView, SIGNAL(doubleClicked(QModelIndex)), SLOT(editQuestion(QModelIndex)));
	
	QDockWidget* informationDock = new QDockWidget(i18n("Information"), this);
	m_questionsInfo = new QLabel(informationDock);
	m_questionsInfo->setAlignment(Qt::AlignTop|Qt::AlignLeft);
	m_questionsInfo->setWordWrap(true);
	informationDock->setWidget(m_questionsInfo);
	addDockWidget(Qt::RightDockWidgetArea, informationDock, Qt::Horizontal);
	
	QDockWidget* tagsDock = new QDockWidget(i18n("All Tags"), this);
	m_tagsView = new QListView(tagsDock);
	m_tagsView->setModel(m_data.tagsModel());
	m_tagsView->setViewMode(QListView::IconMode);
	m_tagsView->setSpacing(5);
	m_tagsView->setSelectionMode(QAbstractItemView::MultiSelection);
	tagsDock->setWidget(m_tagsView);
	addDockWidget(Qt::LeftDockWidgetArea, tagsDock, Qt::Horizontal);
	connect(m_tagsView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), SLOT(tagsSelectionChanged(QItemSelection,QItemSelection)));
	
	setCentralWidget(m_questionsView);
}

Examens::~Examens()
{}

void Examens::newQuestion()
{
	QSet<QString> tags;
	foreach(const QModelIndex& idx, m_tagsView->selectionModel()->selectedIndexes())
		tags += idx.data().toString();
	
	tags += m_lastTags.toSet();
	
	AddQuestion dialog;
	dialog.setTags(tags.toList());
	
	if(dialog.exec()) {
		m_data.addQuestion(dialog.content(), dialog.tags());
		
		m_lastTags=dialog.tags();
	}
}

void Examens::tagsSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
	QModelIndexList rows = m_tagsView->selectionModel()->selectedIndexes();
	
	QStringList tags;
	foreach(const QModelIndex& row, rows) {
		tags += row.data().toString();
	}
	
	m_data.questionsModel()->updateViewedQuestions(tags);
}

void Examens::editQuestion(const QModelIndex& idx)
{
	int id = idx.sibling(idx.row(), 0).data().toInt();
	AddQuestion dialog;
	dialog.setContent(idx.sibling(idx.row(), 1).data().toString());
	dialog.setTags(m_data.tagsForId(id));
	
	if(dialog.exec()) {
		m_data.editQuestion(id, dialog.content(), dialog.tags());
	}
}

void Examens::currentChanged(const QModelIndex& idx, const QModelIndex& prev)
{
	QStringList tags = m_data.tagsForId(idx.sibling(idx.row(),0).data().toInt());
	QString question = m_data.questionForId(idx.sibling(idx.row(),0).data().toInt());
	
	m_questionsInfo->setText(i18n("<b>Question:</b><br/>%1<p/><b>Tags:</b> %2").arg(question).arg(tags.join(i18n(" "))));
}

void Examens::removeSelected()
{
	QModelIndexList idxs = m_questionsView->selectionModel()->selectedRows();
	QMessageBox::StandardButton remove=QMessageBox::warning(this, i18n("Item Removal"), i18n("Are you sure you want to remove %1 questions?").arg(idxs.size()));
		
		foreach(const QModelIndex& idx, idxs) {
			m_data.removeQuestion(idx.sibling(idx.row(), 0).data().toInt());
		}
}

void Examens::exportExam()
{
	QStringList questions;
	QAbstractItemModel* m = m_questionsView->model();
	for(int i=0; i<m->rowCount(); i++) {
		questions += m->index(i, 1).data().toString();
	}
	
	GenerateWizard w(questions);
	if(w.exec()==QDialog::Accepted) {
		QString path = w.path();
		QString extension = path.right(path.size()-path.lastIndexOf('.'));
		
		AbstractExamExport* filter = ExamFilterFactory::self()->filterForExtension(extension);
		bool ret = filter;
		if(filter) {
			QString q;
			int i=1;
			foreach(const QString& qq, w.questions())
				q+=QString("::%1.:: %2\n").arg(i++).arg(qq);
			
			
			filter->exportTo(path, w.format(), q);
		}
		
		if(!ret)
			QMessageBox::warning(this, i18n("Export error"), i18n("Could not export the exam to '%1'").arg(path));
	}
}

#include "examens.moc"
