#ifndef EXAMENSDRIVER_H
#define EXAMENSDRIVER_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQueryModel>

class QuestionsModel : public QSqlQueryModel
{
	Q_OBJECT
	public:
		explicit QuestionsModel(QObject* parent = 0);
		
		void updateViewedQuestions(const QStringList& tags);
		void refresh();
		
	private:
		QString m_query;
};

class ExamensDriver
{
	public:
		ExamensDriver();
		~ExamensDriver();
		
		void addQuestion(const QString& content, const QStringList& tags);
		void editQuestion(int id, const QString& content, const QStringList& tags);
		void removeQuestion(int id);
		
		QStringList tagsForId(int id) const;
		QString questionForId(int id) const;
		
		QSqlQueryModel* tagsModel();
		QuestionsModel* questionsModel();
		
	private:
		void reload();
		
		int maxQuestionsId();
		void initializeDatabase();
		static QString userDatabase();
		
		QSqlDatabase m_db;
		QSqlQueryModel* m_tagsModel;
		QuestionsModel* m_questionsModel;
};

#endif // EXAMENSDRIVER_H
