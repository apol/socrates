#ifndef ADDQUESTION_H
#define ADDQUESTION_H

#include <QDialog>

namespace Ui { class AddQuestion; }

class AddQuestion : public QDialog
{
	Q_OBJECT
	public:
		explicit AddQuestion(QWidget* parent = 0);
		virtual ~AddQuestion();
		
		QString content() const;
		QStringList tags() const;
		void setContent(const QString& content);
		void setTags(const QStringList& tags);
		
	public slots:
		void checkAcceptability();
		
	private:
		Ui::AddQuestion* m_ui;
};

#endif
