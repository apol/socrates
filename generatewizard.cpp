#include "generatewizard.h"
#include "ui_generatewizard.h"
#include <QDebug>
#include <QFileDialog>
#include "examfilterfactory.h"

#define i18n(x) tr(x)

GenerateWizard::GenerateWizard(const QStringList& questions, QWidget* parent)
{
	m_ui = new Ui::GenerateWizard;
	m_ui->setupUi(this);
	connect(this, SIGNAL(currentIdChanged(int)), SLOT(pageChanged(int)));
	
	//First page
	foreach(const QString& q, questions) {
		QListWidgetItem* item = new QListWidgetItem(q, m_ui->selectionList);
		item->setCheckState(Qt::Unchecked);
	}
	
	m_ui->numQuestions->setMaximum(questions.size());
	m_ui->numQuestions->setValue(questions.size());
	
	QAction* all = new QAction(i18n("All questions"), this);
	QAction* rnd = new QAction(i18n("Random Questions"), this);
	m_ui->automaticSelection->addAction(all);
	m_ui->automaticSelection->addAction(rnd);
	
	connect(all, SIGNAL(triggered(bool)), SLOT(selectAll()));
	connect(rnd, SIGNAL(triggered(bool)), SLOT(randomizeSelection()));
	
	//Second page
	m_ui->top->setIcon(QIcon::fromTheme("rating"));
	m_ui->bottom->setIcon(QIcon::fromTheme("process-stop"));
	m_ui->up->setIcon(QIcon::fromTheme("go-up"));
	m_ui->down->setIcon(QIcon::fromTheme("go-down"));
	
	connect(m_ui->up, SIGNAL(clicked(bool)), SLOT(moveUp()));
	connect(m_ui->down, SIGNAL(clicked(bool)), SLOT(moveDown()));
	connect(m_ui->top, SIGNAL(clicked(bool)), SLOT(moveTop()));
	connect(m_ui->bottom, SIGNAL(clicked(bool)), SLOT(moveBottom()));
	
	//Save page
// 	m_ui->wherePage->registerField("path", m_ui->path);
	connect(m_ui->find, SIGNAL(clicked(bool)), SLOT(selectDestinationPath()));
}

GenerateWizard::~GenerateWizard()
{
	delete m_ui;
}

void GenerateWizard::selectAll()
{
	for(int i=0; i<m_ui->selectionList->count() && i<m_ui->numQuestions->value(); i++) {
		QListWidgetItem* item = m_ui->selectionList->item(i);
		item->setCheckState(Qt::Checked);
	}
}

void GenerateWizard::randomizeSelection()
{
	QList<int> its;
	int count = m_ui->selectionList->count();
	for(int i=0; its.size()<m_ui->numQuestions->value(); i++) {
		int elem = qrand() % count;
		if(!its.contains(elem))
			its += elem;
	}
	
	for(int i=0; i<count; i++)
		m_ui->selectionList->item(i )->setCheckState(Qt::Unchecked);
	
	foreach(int it, its)
		m_ui->selectionList->item(it)->setCheckState(Qt::Checked);
}

void GenerateWizard::pageChanged(int id)
{
	if(id==1) {
		m_ui->sortingView->clear();
		
		for(int i=0; i<m_ui->selectionList->count(); i++) {
			QListWidgetItem* item = m_ui->selectionList->item(i);
			if(item->checkState()==Qt::Checked)
				m_ui->sortingView->addItem(item->text());
		}
	}
}

QString GenerateWizard::path() const { return m_ui->path->text(); }
QStringList GenerateWizard::questions() const
{
	QStringList ret;
	for(int i=0; i<m_ui->sortingView->count(); i++)
		ret += m_ui->sortingView->item(i)->text();
	return ret;
}

void GenerateWizard::moveBottom()
{
	int row = m_ui->sortingView->currentRow();
	QListWidgetItem* item = m_ui->sortingView->takeItem(row);
	m_ui->sortingView->addItem(item);
	m_ui->sortingView->setCurrentItem(item);
}

void GenerateWizard::moveTop()
{
	int row = m_ui->sortingView->currentRow();
	QListWidgetItem* item = m_ui->sortingView->takeItem(row);
	m_ui->sortingView->insertItem(0, item);
	m_ui->sortingView->setCurrentItem(item);
}

void GenerateWizard::moveDown()
{
	int row = m_ui->sortingView->currentRow();
	QListWidgetItem* item = m_ui->sortingView->takeItem(row);
	m_ui->sortingView->insertItem(row+1, item);
	m_ui->sortingView->setCurrentItem(item);
}

void GenerateWizard::moveUp()
{
	int row = m_ui->sortingView->currentRow();
	QListWidgetItem* item = m_ui->sortingView->takeItem(row);
	m_ui->sortingView->insertItem(row-1, item);
	m_ui->sortingView->setCurrentItem(item);
}

void GenerateWizard::selectDestinationPath()
{
	m_ui->path->setText(QFileDialog::getSaveFileName(this, i18n("Select where to export the exam..."), QString(), ExamFilterFactory::self()->filters().join(";;")));
}

Format GenerateWizard::format() const
{
	Format f;
	f.topLeft=Qt::escape(m_ui->topLeft->toPlainText());
	f.topCenter=Qt::escape(m_ui->topCenter->toPlainText());
	f.topRight=Qt::escape(m_ui->topRight->toPlainText());
	f.footer=Qt::escape(m_ui->bottomLeft->toPlainText());
	
	f.topLeft.replace('\n', "<br />");
	f.topCenter.replace('\n', "<br />");
	f.topRight.replace('\n', "<br />");
	f.footer.replace('\n', "<br />");
	
	return f;
}

#include "generatewizard.moc"