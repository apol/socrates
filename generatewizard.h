#ifndef GENERATEWIZARD_H
#define GENERATEWIZARD_H

#include <QWizard>
#include "abstractexamexport.h"

namespace Ui { class GenerateWizard; }

class GenerateWizard : public QWizard
{
	Q_OBJECT
	public:
		explicit GenerateWizard(const QStringList& questions, QWidget* parent = 0);
		virtual ~GenerateWizard();
		
		QStringList questions() const;
		QString path() const; //Move to KUrl
	public slots:
		void selectAll();
		void randomizeSelection();
		void pageChanged(int id);
		
		void moveUp();
		void moveTop();
		void moveDown();
		void moveBottom();
		void selectDestinationPath();
		Format format() const;
		
	private:
		Ui::GenerateWizard* m_ui;
};

#endif // GENERATEWIZARD_H
