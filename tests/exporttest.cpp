#include "../examfilterfactory.h"
#include <../abstractexamexport.h>
#include <QDebug>
#include <QDir>

int main()
{
	QStringList exam=QStringList()
	<<
	"// true/false\n"
	"::Q1:: 1+1=2 {T}\n"
	<<
	"// multiple choice with specified feedback for right and wrong answers\n"
	"::Q2:: What's between orange and green in the spectrum? \n"
	"{ =yellow # right; good! ~red # wrong, it's yellow ~blue # wrong, it's yellow }\n"
	<<
	"// fill-in-the-blank\n"
	"::Q3:: Two plus {=two =2} equals four.\n"
	<<
	"// matching\n"
	"::Q4:: Which animal eats which food? { =cat -> cat food =dog -> dog food }\n"
	<<
	"// math range question\n"
	"::Q5:: What is a number from 1 to 5? {#3:2}\n"
	<<
	"// math range specified with interval end points\n"
	"::Q6:: What is a number from 1 to 5? {#1..5}\n"
	"// translated on import to the same as Q5, but unavailable from Moodle question interface\n"
	<<
	"// multiple numeric answers with partial credit and feedback\n"
	"::Q7:: When was Ulysses S. Grant born? {#\n"
	"		=1822:0      # Correct! Full credit.\n"
	"		=%50%1822:2  # He was born in 1822. Half credit for being close.\n"
	"}\n"
	<<
	"// essay\n"
	"::Q8:: How are you? {}\n";
	
	QDir tmp = QDir::temp();
	tmp.mkdir("examenstest");
	tmp.cd("examenstest");
	
	QList<ExamFilterFactory::registerFilter_fn> all = ExamFilterFactory::self()->exporters();
	foreach(ExamFilterFactory::registerFilter_fn func, all) {
		AbstractExamExport* filter = func();
		QString file = tmp.absoluteFilePath("tmpfile"+filter->extension());
		
		Format f;
		f.footer = "tatata";
		
		bool ret = filter->exportTo(file, f, exam.join("\n\n"));
		qDebug() << "Export to " << file << "done. Result: " << ret;
		Q_ASSERT(ret);
	}
	return 0;
}