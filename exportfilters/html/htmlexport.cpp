/*************************************************************************************
 *  Copyright (C) 2007 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "htmlexport.h"
#include "../examfilterfactory.h"
#include <../giftparser/giftparser.h>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QDate>

#define i18n(x) QObject::tr(x)

struct GiftToHtmlVisitor : AbstractGiftVisitor
{
	GiftToHtmlVisitor(QTextStream* ts) : ts(ts) {}
	
	virtual void titleFound(const QString& title) { *ts << "<h3>" << title << "</h3>"; }
	virtual void textFound(const QString& text) { *ts << text; }
	virtual void commentFound(const QString& comment) { *ts << "<!--" << comment.simplified() << "-->\n"; }
	
	virtual void booleanAnswer(bool answer) { *ts << "<em>(True / False)</em> <!-- It is " << (answer ? i18n("True") : i18n("False")) << "-->"; }
	virtual void matchAnswer(const QString& from, const QString& to, const QString& feedback)
	{
		*ts << "<!-- match: " << from << "->" << to << " feedback: " << feedback << "-->\n";
		m_matchAnswers.append(qMakePair(from, to));
	}
	
	virtual void numericalAnswer(bool interval, const QString& value, int percentage, const QString& range, const QString& feedback)
	{
		*ts << "<!-- " << value << (interval?"..":":") << range << " ponders " << percentage << "% feedback: " << feedback << "-->\n";
	}
	
	virtual void testAnswer(bool correct, int percentage, const QString& text, const QString& feedback)
	{
		testOptions += text;
		allCorrect &= correct;
		
		if(correct)
			*ts << "<!-- " << text << " is correct! feedback: "<< feedback << " -->\n";
		else if(percentage>0)
			*ts << "<!-- " << text << " is " << percentage << " correct! feedback: "<< feedback << " -->\n";
		else
			*ts << "<!-- " << text << " is wrong! feedback: "<< feedback << " -->\n";
	}
	
	virtual void openQuestion(bool numerical) { if(numerical) printField(); allCorrect=true; }
	virtual void closeQuestion()
	{
		if(!testOptions.isEmpty()) {
			if(!allCorrect) {
				*ts << "<em>(" << testOptions.join(" / ") << ")</em>";
			} else {
				printField();
			}
			testOptions.clear();
		} else if(!m_matchAnswers.isEmpty()) {
			QStringList from, to;
			typedef QPair<QString,QString> QStringString ;
			
			foreach(const QStringString& ans, m_matchAnswers)
			{ from+=ans.first; to+=ans.second; }
			
			*ts << "<table>\n";
			while(!from.isEmpty()) {
				QString cellfrom = from.takeAt(qrand()%from.size());
				QString cellto = to.takeAt(qrand()%to.size());
				
				*ts << "\t<tr><td>" << cellfrom <<"</td><td>"<< cellto <<"</td></tr>";
			}
			*ts << "</table>\n";
			
			m_matchAnswers.clear();
		}
	}
	
	virtual void error(const QString& error) { m_error = error; qDebug() <<"error!!" << error; }
	
	QTextStream* ts;
	QString m_error;
	
	QStringList testOptions;
	QList<QPair<QString, QString> > m_matchAnswers;
	bool allCorrect;
	void printField() { *ts << " _____"; }
};

bool HtmlExport::exportTo(const QString& path, const Format& header, const QString& q)
{
	qsrand(QTime::currentTime().msec());
	QFile f(path);
	if(!f.open(QFile::WriteOnly)) {
		qDebug() << "Couldn't write " << path;
		return false;
	}
	QTextStream ts(&f);
	
	ts << "<html>\n<head>\n"
			"\t<title>Some Exam</title>\n"
			"\t<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n"
			"</head>\n<body>\n";
	ts << "<p class='header'><table><tr><td>"; 
	ts << "<td>" << header.topLeft << "</td>";
	ts << "<td>" << header.topCenter << "</td>";
	ts << "<td>" << header.topRight << "</td>";
	ts << "</td></tr></table></p>\n";
	
	GiftToHtmlVisitor v(&ts);
	
	QString qq(q);
	QTextStream input(&qq);
	GiftParser p(&input);
	p.start(&v);
	
	ts << "<td>" << header.footer << "</td>";
	ts << "</body>\n</html>\n";
	
	return true;
}

REGISTER_EXPORT_FILTER(HtmlExport)
