/*************************************************************************************
 *  Copyright (C) 2007 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "odfexport.h"
#include "../examfilterfactory.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QTextDocument>
#include <QTextDocumentWriter>
#include <QTextCursor>
#include <QTextTable>
#include <../giftparser/giftparser.h>

#define i18n(x) QObject::tr(x)

struct GiftToOdfVisitor : AbstractGiftVisitor
{
	GiftToOdfVisitor(QTextCursor* cursor) : m_cursor(cursor) {}
	
	virtual void titleFound(const QString& title) { m_cursor->insertBlock(); m_cursor->insertText(title, titleFormat()); }
	virtual void textFound(const QString& text) { m_cursor->insertText(text, textFormat()); }
	virtual void commentFound(const QString& comment) {}
	
	virtual void booleanAnswer(bool answer) { m_cursor->insertText(i18n("(True / False)"), answerFormat()); }
	virtual void matchAnswer(const QString& from, const QString& to, const QString& feedback) { m_matchAnswers.append(qMakePair(from, to)); }
	virtual void numericalAnswer(bool interval, const QString& value, int percentage, const QString& range, const QString& feedback) {}
	virtual void testAnswer(bool correct, int percentage, const QString& text, const QString& feedback)
	{
		testOptions += text;
		allCorrect &= correct;
	}
	
	virtual void openQuestion(bool numerical) { allCorrect=true; if(numerical) printField(); }
	virtual void closeQuestion()
	{
		if(!testOptions.isEmpty()) {
			if(!allCorrect) {
				m_cursor->insertText("(" + testOptions.join(" / ") + ")", answerFormat());
			} else {
				printField();
			}
			testOptions.clear();
		} else if(!m_matchAnswers.isEmpty()) {
			QStringList from, to;
			typedef QPair<QString,QString> QStringString ;
			
			foreach(const QStringString& ans, m_matchAnswers)
			{ from+=ans.first; to+=ans.second; }
			
			QTextTable* table = m_cursor->insertTable(from.size(), 2);
			while(!from.isEmpty()) {
				QString cellfrom = from.takeAt(qrand()%from.size());
				QString cellto = to.takeAt(qrand()%to.size());
				
				m_cursor->insertText(cellfrom);
				m_cursor->movePosition(QTextCursor::NextCell);
				m_cursor->insertText(cellto);
				m_cursor->movePosition(QTextCursor::NextCell);
			}
			m_cursor->movePosition(QTextCursor::NextBlock);
			
			m_matchAnswers.clear();
		}
	}
	
	virtual void error(const QString& error) { m_error = error; }
	
	QTextCharFormat titleFormat()
	{
		QTextCharFormat ret;
		ret.setFontWeight(QFont::Bold);
		ret.setFontPointSize(15.);
		return ret;
	}
	
	QTextCharFormat answerFormat()
	{
		QTextCharFormat ret;
		ret.setFontItalic(true);
		return ret;
	}
	
	QTextCharFormat textFormat()
	{
		return QTextCharFormat();
	}
	
	bool allCorrect;
	QStringList testOptions;
	QList<QPair<QString, QString> > m_matchAnswers;
	void printField() { m_cursor->insertText("_______"); }
	
	QTextCharFormat boolean;
	QTextCursor* m_cursor;
	QString m_error;
};

bool OdfExport::exportTo(const QString& path, const Format& header, const QString& q)
{
	QTextDocument doc;
	QTextCursor cursor(&doc);
	QTextTable* tt=cursor.insertTable(1,3);
	cursor.insertHtml(header.topLeft);
	cursor.movePosition(QTextCursor::NextCell);
	cursor.insertHtml(header.topCenter);
	cursor.movePosition(QTextCursor::NextCell);
	cursor.insertHtml(header.topRight);
	cursor.movePosition(QTextCursor::NextBlock);
	
	cursor.insertHtml("<br />\n");
	
	QString qq(q);
	QTextStream input(&qq);
	
	GiftToOdfVisitor v(&cursor);
	GiftParser p(&input);
	p.start(&v);
	
	if(v.m_error.isEmpty()) {
		cursor.insertHtml("<br />\n");
		cursor.insertHtml(header.footer);
		QTextDocumentWriter tdw(path, "odf");
		return tdw.write(&doc);
	}
	
	return false;
}

REGISTER_EXPORT_FILTER(OdfExport)
