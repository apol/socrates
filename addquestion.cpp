#include "addquestion.h"
#include "ui_addquestion.h"
#include <QPushButton>

AddQuestion::AddQuestion(QWidget* parent)
	: QDialog(parent)
{
	m_ui = new Ui::AddQuestion;
	m_ui->setupUi(this);
	
	QRegExpValidator* rxValidator = new QRegExpValidator(this);
	rxValidator->setRegExp(QRegExp("[0-9A-Za-z ]*"));
	m_ui->tags->setValidator(rxValidator);
	
	connect(m_ui->content, SIGNAL(textChanged()), SLOT(checkAcceptability()));
	connect(m_ui->tags, SIGNAL(textChanged(QString)), SLOT(checkAcceptability()));
	checkAcceptability();
}

AddQuestion::~AddQuestion()
{
	delete m_ui;
}

void AddQuestion::checkAcceptability()
{
	bool valid = (!m_ui->content->toPlainText().isEmpty() && !m_ui->tags->text().isEmpty() ) || m_ui->tags->isModified();
	m_ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(valid);
}

QString AddQuestion::content() const { return m_ui->content->toPlainText(); }
QStringList AddQuestion::tags() const { return m_ui->tags->text().split(' ', QString::SkipEmptyParts); }

void AddQuestion::setContent(const QString& content) { m_ui->content->setText(content);}
void AddQuestion::setTags(const QStringList& tags) { m_ui->tags->setText(tags.join(" "));}

#include "addquestion.moc"