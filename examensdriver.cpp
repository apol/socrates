#include "examensdriver.h"
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtGui/QDesktopServices>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QDebug>
#include <QVariant>
#include <QSqlQueryModel>

ExamensDriver::ExamensDriver()
{
	initializeDatabase();
	
	m_tagsModel=new QSqlQueryModel;
	m_questionsModel=new QuestionsModel;
	reload();
}

ExamensDriver::~ExamensDriver()
{
	delete m_tagsModel;
	delete m_questionsModel;
}

void ExamensDriver::initializeDatabase()
{
	bool exists = QFile::exists(userDatabase());
	
	m_db = QSqlDatabase::addDatabase("QSQLITE");
	m_db.setDatabaseName(userDatabase());
	
	bool b = m_db.open();
	if(!b)
		qDebug() << "Error" << m_db.lastError();
	Q_ASSERT(b);
	
	if(!exists) {
		qDebug() << "Initializing database. Creating tables...";
		QSqlQuery query(m_db);
		bool ret = query.exec("CREATE TABLE questions (id INTEGER PRIMARY KEY, content VARCHAR(200));");
		ret &= query.exec("CREATE TABLE tags (id INTEGER, tag VARCHAR(20));");
		
		Q_ASSERT(ret);
	}
}

QString ExamensDriver::userDatabase()
{
	QString dir = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
	QDir::temp().mkpath(dir);
	
	dir += "/exams.db";
	
	qDebug() << "using database: " << dir;
	return dir;
}

void ExamensDriver::addQuestion(const QString& content, const QStringList& tags)
{
	QSqlQuery addq("INSERT INTO questions VALUES (NULL, :content);", m_db);
	addq.bindValue(":content", content);
	bool b = addq.exec();
	if(!b)
		qDebug() << "Error after adding: " << m_db.lastError();
	Q_ASSERT(b);
	
	int maxid = maxQuestionsId();
	foreach(const QString& tag, tags) {
		QSqlQuery addq("INSERT INTO tags VALUES (:id, :tag);", m_db);
		addq.bindValue(":id", maxid);
		addq.bindValue(":tag", tag);
		bool b = addq.exec();
		
		Q_ASSERT(b);
	}
	reload();
}

int ExamensDriver::maxQuestionsId()
{
	QSqlQuery addq("SELECT max(id) FROM questions;", m_db);
	bool b = addq.exec();
	if(!b)
		qDebug() << "Error after adding: " << m_db.lastError();
	Q_ASSERT(b);
	
	int ret=-1;
	if(addq.next())
		ret = addq.value(0).toInt();
	
	Q_ASSERT(ret>=0);
	return ret;
}

QStringList ExamensDriver::tagsForId(int id) const
{
	QSqlQuery addq;
	addq.prepare("SELECT DISTINCT tag FROM tags WHERE id=:id;");
	addq.bindValue(":id", id);
	bool b = addq.exec();
	Q_ASSERT(b);
	
	QStringList ret;
	while (addq.next())
		ret += addq.value(0).toString();
	
	qDebug() << "tags for" << id << ret;
	return ret;
}

QString ExamensDriver::questionForId(int id) const
{
	QSqlQuery addq;
	addq.prepare("SELECT content FROM questions WHERE id=:id;");
	addq.bindValue(":id", id);
	bool b = addq.exec();
	Q_ASSERT(b);
	
	QString ret;
	addq.next();
	ret = addq.value(0).toString();
	Q_ASSERT(!addq.next());
	
	return ret;
}

void ExamensDriver::editQuestion(int id, const QString& content, const QStringList& tags)
{
	QSqlQuery edcontent;
	edcontent.prepare("UPDATE questions SET content=:content WHERE id=:id ;");
	edcontent.bindValue(":content", content);
	edcontent.bindValue(":id", id);
	bool b = edcontent.exec();
	if(!b)
		qDebug() << "error" << m_db.lastError();
	Q_ASSERT(b);
	
	//We remove all tags and add them again, probably could be more conservative. TODO?
	QSqlQuery remtags;
	remtags.prepare("DELETE FROM tags WHERE id=:id;");
	remtags.bindValue(":id", id);
	b=remtags.exec();
	Q_ASSERT(b);
	
	foreach(const QString& tag, tags) {
		QSqlQuery addq("INSERT INTO tags VALUES (:id, :tag);");
		addq.bindValue(":id", id);
		addq.bindValue(":tag", tag);
		b = addq.exec();
		
		Q_ASSERT(b);
	}
	reload();
}

void ExamensDriver::removeQuestion(int id)
{
	QSqlQuery remQ;
	remQ.prepare("DELETE FROM questions WHERE id=:id;");
	remQ.bindValue(":id", id);
	bool b = remQ.exec();
	if(!b)
		qDebug() << "error:" << remQ.lastError();
	Q_ASSERT(b);
	
	remQ.prepare("DELETE FROM tags WHERE id=:id;");
	remQ.bindValue(":id", id);
	b = remQ.exec();
	if(!b)
		qDebug() << "error:" << remQ.lastError();
	Q_ASSERT(b);
	
	reload();
}

void ExamensDriver::reload()
{
	m_tagsModel->setQuery("SELECT DISTINCT tag FROM tags;");
	m_questionsModel->refresh();
}

QuestionsModel::QuestionsModel(QObject* parent)
	: QSqlQueryModel(parent)
{
	updateViewedQuestions(QStringList());
}

void QuestionsModel::updateViewedQuestions(const QStringList& tags)
{
	QString tagCondition;
	foreach(const QString& tag, tags) {
		tagCondition += tagCondition.isEmpty() ? "WHERE" : "AND";
		tagCondition += QString(" EXISTS (SELECT * FROM tags t WHERE q.id=t.id AND t.tag=\"%1\") ").arg(tag);
	}
	m_query = "SELECT q.id, q.content FROM questions q "+tagCondition+';';
	refresh();
}

void QuestionsModel::refresh()
{
	QSqlQuery q(m_query);
	q.exec();
	
// 	Q_ASSERT(q.isActive());
	setQuery(q);
	
	qDebug() << "weeeeeee" << q.lastError() << q.lastQuery();
}

QSqlQueryModel* ExamensDriver::tagsModel() { return m_tagsModel; }
QuestionsModel* ExamensDriver::questionsModel() { return m_questionsModel; }

#include "examensdriver.moc"
